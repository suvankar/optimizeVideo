```
DESCRIPTION
       Converts  any  video  to  its smallest size version which does not have
       perceivable quality loss, while still fast to convert.

       After the conversion the original video is removed.

       But the original video won't be substituted if its format is  webm  and
       the compression gain would be less than 15%.

USAGE
          ON THE FILE MANAGER
              Right clic on the video, and select open with optimizeVideo.

          ON THE TERMINAL
              optimizeVideo [videos]
